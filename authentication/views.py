from django.shortcuts import redirect, render
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth import authenticate, login, logout
from .models import CreateUserForm

def login_view(request):
    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = authenticate(request, username=username, password=password)
        if user:
            login(request, user)
            return redirect('profile')
        context = "Invalid username and password"
        return render(request, 'accounts/login.html', {'form': AuthenticationForm(), 'error': context})
    return render(request, 'accounts/login.html', {'form': AuthenticationForm()})

def profile_view(request):
    return render(request, 'accounts/profile.html')

def logout_view(request):
    logout(request)
    return render(request, 'accounts/logout.html')

def register_view(request):
    form = CreateUserForm()
    context = {}

    if request.method == 'POST':
        form = CreateUserForm(request.POST)
        if form.is_valid():
            form.save()
            new_user = authenticate(username=form.cleaned_data['username'],
                                    password=form.cleaned_data['password1'],
                                    )
            login(request, new_user)
            return redirect('profile')
    
    context['form'] = form
    return render(request, 'accounts/register.html', context)